"""
Usage: main.py [--outfile=<filename>]
"""
def func():
    return "Hello"

def func2():
    return "World! "

def main():
    import docopt

    args = docopt.docopt(__doc__)
    if args["--outfile"] is not None:
        with open(args["--outfile"], "w") as fp: 
            while True:
                try:
                    print(input(), file=fp)
                except EOFError:
                    return
    
    while True:
        try:
            print(input())
        except EOFError:
            return

if __name__ == "__main__":
    main()