#!/bin/bash
######## Variable globals -> ########

# The language that the assignment is written in.  Currently supported
# options are "cpp", "python", "bash"
language="python"

# Specify the interpreter, or empty string for compiled binaries
interpreter="python3"

# The file containing the "main" entry point for the program.
# In C or C++, this is the name of the executable
main_file="main.py"

# Any arguments you want passed to the main driver, upon excution.
# If you do not have any arguments, just leave as an empty string, ""
main_file_arguments=""

# Command to build main from source
build_command=""

# Whether or not to score the student using a static analyzer.
# For Python, this is the mypy type-checker.
# For C or C++, this is cppcheck.
# For Bash, this is shellcheck
static_analysis_command="mypy *.py --strict --disallow-any-explicit"

# Whether or not to score the student using an autoformatter (dock points
# if not formatted correctly).
# For Python, this is black.
# For C++, this is clang-format.
# For Bash, this is shfmt
format_command="black *.py --check"

# Whether or not to use fuzzy or ridig diffs
# If you choose true, fuzzy diffs will give partial credit.
# This can be helpful for string-heavy assignments,
# where correctness is reasonable to estimate statistically.
# If you choose false, rigid diffs will be all-or-none.
# This is helpful when the assignment is mathy,
# where correctness is not reasonable to estimate statistically.
fuzzy_partial_credit=false

# The timeout duration in seconds for killing a student process.
# This can limit infinite run-times.
# For computationally expensive operations, you may increase this time as desired.
process_timeout=15

# The file to which the final grade is written
# This file can be saved as as artifact in gitlb CI/CD, and used to upload scores to Canvas using assigner.
student_file="results.txt"
######## <- Variable globals ########

######## File and type existence tests -> ########
# Load the specified assosicative array with files you want to check for the existence of.
# Key is the file, and Value must be a sub-string within what is produced by the bash command:
# $ file file.whatever
declare -A file_arr
# file_arr=(
#     ["ssh-key0.png"]="PNG image data"
#     ["id_rsa.pub"]="OpenSSH RSA public key"
#     ["gpg2_key.asc"]="PGP public key block Public-Key"
# )
######## <- File and type existence tests ########

######## Custom tests -> ########
# Any tests other than the unit tests and the stdout diff tests belong here,
# and must be bash functions whose names begin with "custom_test".
# Custom tests should report their score by assigning their result (0-100)
# to the custom_test_score, e.g.:
# custom_test_score=100
# Custom tests are performed alphabetically,
# so number them if you want them in order.

# custom_test_1_basic() {
#     # Do your stuff here, to determine the grade
#     echo "Custom test internals here"
#     custom_test_score=100
# }
######## <- Custom tests ########

if [ ! -d .admin_files ]; then
    git clone --recurse-submodules https://gitlab.com/classroomcode/grade-sh/admin_files .admin_files 
    (   
        cd .admin_files
        if [ "$1" = "-t" ]; then
            if ! git checkout "$2"; then
                echo "Failed to pull branch $2"
                exit 1
            fi
        fi
        ./setup.sh
    )
    # rm -rf .admin_files/.git
    exit 0
fi

# Initialize database and set test class attributes
# --priority sets the order in which testclass are run
# --weight sets the weight of each test within the testclass
source .admin_files/database-interface.sh

database.start
database.testattr unit_tests     --priority=1
database.testattr arg_tests      --priority=2
database.testattr stdio_tests    --priority=99   --weight=2
database.testattr static_test    --priority=100
database.testattr format_test    --priority=101

source .admin_files/main.sh "$@"
