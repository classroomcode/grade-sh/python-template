import sys
import os

sys.path.append(os.getcwd())

import main

assert 1 == 1
assert "World!" == main.func2(), "Try fixing in main.py"
assert 2 == 2
assert 0 == 1
